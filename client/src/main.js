import Vue from 'vue'
import axios from 'axios'
import VueAxios from 'vue-axios'
import Vuex from 'vuex'
import BootstrapVue	from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
//import AsyncComputed from 'vue-async-computed'

import App from './App.vue'
import router from './router'
import BootstrapVueIcons from 'bootstrap-vue'
import CryptoWrapper from './utils/cryptoWrapper.js';

Vue.use(VueAxios, axios)

Vue.axios.defaults.withCredentials = true;
Vue.use(BootstrapVueIcons)
Vue.use(BootstrapVue)
Vue.use(Vuex)
//Vue.use(AsyncComputed)



// import vueMoment from 'vue-moment'
// Vue.use(vueMoment)

Vue.use(CryptoWrapper);

Vue.config.productionTip = false
Vue.config.errorHandler = function(err, vm, info) {
	window.console.log(`>> App >> errorHandler > Error: ${err.toString()}\nInfo: ${info}`);
}
Vue.config.warnHandler = function(msg, vm, trace) {
	window.console.log(`>> App >> warnHandler > Warn: ${msg}\nTrace: ${trace}`);
}

Vue.prototype.$API = process.env.VUE_APP_API;

const store = new Vuex.Store({
	state:{
		user_id: -1,
		_id: -1,
		privilege: -1,
		msgError: null,
		msgSuccess: null,
		msgWarning: null,
		username: null,
		instance: {
			_id: 0,
			title: "loading...",
			description: "loading..."
		},
		profileImg: null
	},
	mutations:{
		logIn (state, user){
			window.console.log("Vuex Store >> user logged in",user);
			state.user_id = user._id
			state._id = user._id
			state.privilege = user.privilege
			state.username = user.username
			state.profileImg = user.profileImg
			window.console.log("Vuex Store >> user_id",state.user_id);
		},
		logOut (state){
			window.console.log("Vuex Store >> user logged out");
			state.user_id = -1
			state.privilege = -1
			state.username = null
		},
		setInstance(state, instance){
			window.console.log("Vuex Store >> instance set ",instance);
			state.instance = instance
		},
		unsetInstance(state){
			window.console.log("Vuex Store >> no instance");
			state.instance = null;
		},
		alertError(state, msg){
			state.msgError = msg;
			setTimeout(() => { state.msgError = null },4000)
		},
		alertSuccess(state, msg){
			state.msgSuccess = msg;
			setTimeout(() => { state.msgSuccess = null },4000)
		},
		alertWarning(state, msg){
			state.msgWarning = msg;
			setTimeout(() => { state.msgWarning = null },4000)
		}
	},
	getters:{
		loggedIn: state => {
			return (state.user_id !== -1)
		}
	}
})


const vm = new Vue({
	store,
	render: h => h(App),
	router
})
vm.$mount('#app')
