const fs = require('fs')

module.exports = {
	css: {
		loaderOptions: {
			scss: {
				prependData: `@import "~@/variables.scss";`
			}
		}
	},
	devServer: {
	    open: process.platform === 'darwin',
	    host: '0.0.0.0',
	    port: 8080, // CHANGE YOUR PORT HERE!
	    https: {
			key: fs.readFileSync('./security/local-key.pem'),
        	cert: fs.readFileSync('./security/local.pem'),
	    },
	    hotOnly: false,
	},
	configureWebpack: {
		plugins: [
		],
		module: {
			rules: [
				{ 
					test: /\.wasm$/,
					type: 'javascript/auto',
					loader: 'file-loader',
					options: {
						name: '[name]-[hash].[ext]',         
					}        
					
				}
			]
		}
	}

	
}